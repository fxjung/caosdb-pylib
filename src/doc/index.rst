
Welcome to PyCaosDB's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Getting started <README_SETUP>
   tutorials/index
   Concepts <concepts>
   Configuration <configuration>
   Administration <administration>
   API documentation<_apidoc/caosdb>

This is the documentation for the Python client library for CaosDB, ``PyCaosDB``.

This documentation helps you to :doc:`get started<README_SETUP>`, explains the most important
:doc:`concepts<concepts>` and offers a range of :doc:`tutorials<tutorials/index>`.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
